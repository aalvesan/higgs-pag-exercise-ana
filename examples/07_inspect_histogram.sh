#!/bin/sh
action () {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    source ${this_dir}/common.sh

    args=(
        $CF_STORE_LOCAL/analysis_h4l/cf.MergeHistograms/$my_config/$my_sig_dataset/nominal/calib__default/sel__default/prod__default/$my_version/hist__jet1_pt.pickle
    )

    if [ ! -f "${args[0]}" ]; then
        echo "ERROR: file not found: ${args[0]}"
        echo "Run examples/04_plot_jet1_pt.sh to produce it first."
        return 1
    fi

    cf_inspect "${args[@]}"
}

action "$@"

